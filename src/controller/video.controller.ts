import * as _ from 'lodash';

import VideoJsonResponseModel from './../models/videoJsonResponse.model';

import DataService from './../services/data.service';

export default class VideoController extends DataService {
	// TODO - add cache management
	async getPlayersFromFolder(token: string): Promise<VideoJsonResponseModel> { // TODO - return VideoModel[]
		const url = `https://api.vidyard.com/dashboard/v1/players?auth_token=${token}`;

		try {
			const {data} = await VideoController.getJsonDataByUrl(url);

			return this._parseVideos(data);
		} catch ({response}) {
			throw {
				status: response.status,
				message: response.data.error
			}
		}
	}

	_parseVideos(playersObject: any): VideoJsonResponseModel { // TODO - return VideoModel[], we don't have object d.ts from vidyard
		const {players} = playersObject;
		const videosObject = players.reduce((acc: VideoJsonResponseModel, videoDetails: any) => {


			acc.videos.push({
				name: videoDetails.name,
				chapters: videoDetails.chapters_attributes,
				autoplay: videoDetails.autoplay,
				description: videoDetails.description,
				default_hd: videoDetails.default_hd,
				caption_display: videoDetails.caption_display,
				sync: videoDetails.sync,
				sync_thumbnail: videoDetails.sync_thumbnail,
				current_thumbnail_id: videoDetails.current_thumbnail_id,
				tags_attributes: videoDetails.tags_attributes,
				seconds: videoDetails.length_in_seconds,
				milliseconds: videoDetails.milliseconds,
				id: videoDetails.id,
				uuid: videoDetails.uuid,
				error_message: videoDetails.error_message,
				created_at: videoDetails.created_at,
				updated_at: videoDetails.updated_at,
				is_secure: videoDetails.is_secure,
				sync_captions: videoDetails.sync_captions,
				video_type: videoDetails.video_type,
				disable_playlist: videoDetails.disable_playlist,
				player_type: videoDetails.player_type,
				width: videoDetails.width,
				height: videoDetails.height,
				captions: videoDetails.captions,
				// NOT DOCUMENTED IN VIDYARD API
				thumbnail: videoDetails.uuid ? `https://play.vidyard.com/${videoDetails.uuid}.jpg` : '',
				// CUSTOM FIELD
				speakers: videoDetails.speakers || [],
			});


			// TODO - refactor in order not to create uniq with every iteration
			acc.tags = [...acc.tags, ...videoDetails.tags_attributes];
			acc.tags = _.uniqBy(acc.tags, 'id');

			return acc;
		}, {videos: [], tags: []});

		if (videosObject && videosObject.videos && videosObject.videos.length) {
			return videosObject;
		} else {
			throw {
				status: 400,
				message: 'No videos found',
			}
		}
	}
}
