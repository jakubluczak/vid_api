import dotenv from 'dotenv';
import express, { Request, Response } from 'express';
// @ts-ignore
import VideoController from './controller/video.controller';
import VideoJsonResponseModel from "./models/videoJsonResponse.model";


// TODO - classify entire file
const videoController:VideoController = new VideoController();


const app = express();
dotenv.config();

// TODO - get token from AEM;
// const token = 'X-FmIK3avQFdIfO7NKHdqA';
const token = 'eJPwOo-CS0G8q8s6xOEDYw';

app.get('/api/videos.json',  async (req:Request, res:Response) => {
	try {
		const videos:VideoJsonResponseModel = await videoController.getPlayersFromFolder(token);

		res.send(videos)
	} catch (err) {
		if (err.status) {
			res.status(err.status).send(err.message);
		} else {
			res.status(500).send();
		}
	}
});

app.listen(process.env.SERVER_PORT, () => {
	if (process.env.ENVIRONMENT==='development') {
		console.log(`server is listening on port ${process.env.SERVER_PORT}`);
	}
});
