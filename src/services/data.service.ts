import axios from 'axios';

export default class DataService {
	static getJsonDataByUrl(url: string): Promise<any> {
		return axios({
			url,
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			}
		})
	}
}
