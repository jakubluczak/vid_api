export default class VideoModel {
	readonly name: string;
	readonly id: number;
	readonly organization_id: number;
}
