import TagModel from './tags.model';
import VideoModel from './video.model'

export default class VideoJsonResponseModel {
	videos: VideoModel[];
	tags: TagModel[];

}
