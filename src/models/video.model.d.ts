import TagsModel from './tags.model';

export default class VideoModel {
	readonly name: string;
	readonly autoplay: string;
	readonly description: string;
	readonly default_hd: string;
	readonly caption_display: string;
	readonly sync: string;
	readonly sync_thumbnail: string;
	readonly current_thumbnail_id: string;
	readonly tags_attributes: TagsModel[];
	readonly seconds: string;
	readonly milliseconds: string;
	readonly id: string;
	readonly uuid: string;
	readonly error_message: string;
	readonly created_at: string;
	readonly updated_at: string;
	readonly is_secure: string;
	readonly sync_captions: string;
	readonly video_type: string;
	readonly disable_playlist: string;
	readonly player_type: string;
	readonly width: string;
	readonly height: string;
	readonly captions: string;
	readonly thumbnail: string;
	readonly chapters: [{
		chapterTitle: string,
		chapterSeconds: number
	}];
	readonly speakers: {
		speakerName: string,
		speakerBio: string
	}
}
